package sqlite // import "modernc.org/sqlite"

import (
	"database/sql"
	"fmt"
	"sync"
	"sync/atomic"
	"unsafe"

	"modernc.org/libc"
	sqlite3 "modernc.org/sqlite/lib"
)

func AddFTS5Tokenizer(connection *sql.Conn, name string, tokenizer FTS5TokenizerFactory) (err error) {
	mu.Lock()
	defer mu.Unlock()

	if err = connection.Raw(func(driverConn interface{}) error {
		c := driverConn.(*conn)
		err = createTokenizer(c, name, tokenizer)
		if err != nil {
			return err
		}

		return nil
	}); err != nil {
		return
	}
	return
}

func RegisterFTS5Tokenizer(name string, tokenizer FTS5TokenizerFactory) error {
	d.hooks = append(d.hooks, func(c *conn) error {
		return createTokenizer(c, name, tokenizer)
	})
	return nil
}

func createTokenizer(c *conn, name string, tokenizer FTS5TokenizerFactory) error {
	tls := c.tls
	sql, err := libc.CString("SELECT fts5(?1)")
	if err != nil {
		return err
	}

	stmt, err := c.prepareV2(&sql)
	if err != nil {
		return err
	}

	ptrType, err := libc.CString("fts5_api_ptr")
	if err != nil {
		return err
	}

	var fts5 uintptr
	sqlite3.Xsqlite3_bind_pointer(c.tls, stmt, 1, uintptr(unsafe.Pointer(&fts5)), ptrType, 0)
	sqlite3.Xsqlite3_step(c.tls, stmt)
	rc := sqlite3.Xsqlite3_finalize(c.tls, stmt)
	if rc != sqlite3.SQLITE_OK {
		return fts5Error{code: rc}
	}

	api := (*sqlite3.Fts5_api)(unsafe.Pointer(fts5))
	zName, err := libc.CString(name)
	if err != nil {
		return err
	}

	createf := (*struct {
		f func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr) int32
	})(unsafe.Pointer(&struct{ uintptr }{api.FxCreateTokenizer})).f

	userData := addObject(tokenizer)
	rc = createf(tls, fts5, zName, userData, uintptr(unsafe.Pointer(&fts5Tokenizer)), xDestroy)
	if rc != sqlite3.SQLITE_OK {
		return fts5Error{code: rc}
	}
	return nil
}

var fts5Tokenizer = sqlite3.Fts5_tokenizer{}
var xDestroy uintptr

type fts5Error struct {
	code int32
}

func (e fts5Error) Error() string {
	return fmt.Sprintf("sqlite3 error: %d", e.code)
}

func fts5Create(tls *libc.TLS, userData uintptr, pArgs uintptr, nArg int32, pTokenizer uintptr) int32 {
	t := getObject(userData)
	if t == nil {
		panic("internal error")
	}

	args := make([]string, int(nArg))
	p := *(*uintptr)(unsafe.Pointer(pArgs))
	for i := 0; i < int(nArg); i++ {
		arg := libc.GoString(p + unsafe.Sizeof(uintptr(0))*uintptr(i))
		args[i] = arg
	}

	tf := t.(FTS5TokenizerFactory)
	tokenizer := tf.Create(args)
	h := addObject(tokenizer)
	*(*uintptr)(unsafe.Pointer(pTokenizer)) = h
	print("create tokenizer", h)
	return sqlite3.SQLITE_OK
}

func fts5Delete(tls *libc.TLS, tokenizer uintptr) int32 {
	removeObject(tokenizer)
	print("delete tokenizer", tokenizer)
	return sqlite3.SQLITE_OK
}

func fts5Tokenize(tls *libc.TLS, tok uintptr, pctx uintptr, tflags int32, pText uintptr, nText int32, xToken uintptr) int32 {
	t := getObject(tok)
	if t == nil {
		panic("internal error")
	}

	tokenizer := t.(FTS5Tokenizer)
	text := libc.GoBytes(pText, int(nText))
	tokens, err := tokenizer.Tokenize(text, tflags)
	if err != nil {
		return sqlite3.SQLITE_ERROR
	}

	if xToken != 0 {
		cb := (*struct {
			f func(*libc.TLS, uintptr, int32, uintptr, int32, int32, int32) int32
		})(unsafe.Pointer(&struct{ uintptr }{xToken})).f

		for _, token := range tokens {
			pToken, _ := libc.CString(token.Token)
			rc := cb(tls, pctx, int32(token.Flags), pToken, int32(len(token.Token)), token.Start, token.End)
			if rc != sqlite3.SQLITE_OK {
				return rc
			}
			libc.Xfree(tls, pToken)
		}
	}

	return sqlite3.SQLITE_OK
}

func fts5Destroy(tls *libc.TLS, pctx uintptr) int32 {
	fmt.Printf("destroy: %d\n", pctx)
	removeObject(pctx)
	return 0
}

const (
	FTS5_TOKENIZE_DOCUMENT = sqlite3.FTS5_TOKENIZE_DOCUMENT
	FTS5_TOKENIZE_QUERY    = sqlite3.FTS5_TOKENIZE_QUERY
	FTS5_TOKENIZE_PREFIX   = sqlite3.FTS5_TOKENIZE_PREFIX
	FTS5_TOKENIZE_AUX      = sqlite3.FTS5_TOKENIZE_AUX
	FTS5_TOKEN_COLOCATED   = sqlite3.FTS5_TOKEN_COLOCATED
)

func init() {
	*(*func(*libc.TLS, uintptr, uintptr, int32, uintptr) int32)(unsafe.Pointer(uintptr(unsafe.Pointer(&fts5Tokenizer.FxCreate)))) = fts5Create
	*(*func(*libc.TLS, uintptr) int32)(unsafe.Pointer(uintptr(unsafe.Pointer(&fts5Tokenizer.FxDelete)))) = fts5Delete
	*(*func(*libc.TLS, uintptr, uintptr, int32, uintptr, int32, uintptr) int32)(unsafe.Pointer(uintptr(unsafe.Pointer(&fts5Tokenizer.FxTokenize)))) = fts5Tokenize
	*(*func(*libc.TLS, uintptr) int32)(unsafe.Pointer(uintptr(unsafe.Pointer(&xDestroy)))) = fts5Destroy
}

type FTS5Token struct {
	Token string
	Flags int32
	Start int32
	End   int32
	Type  int
}

type FTS5Tokenizer interface {
	Tokenize(text []byte, flags int32) (tokens []*FTS5Token, err error)
}

type FTS5TokenizerFactory interface {
	Create(args []string) FTS5Tokenizer
	Delete(FTS5Tokenizer)
}

///
var (
	fToken uintptr

	// New, FS.Close
	mu sync.Mutex

	objectMu sync.Mutex
	objects  = map[uintptr]interface{}{}
)

func token() uintptr { return atomic.AddUintptr(&fToken, 1) }

func addObject(o interface{}) uintptr {
	t := token()
	objectMu.Lock()
	objects[t] = o
	objectMu.Unlock()
	return t
}

func getObject(t uintptr) interface{} {
	objectMu.Lock()
	o := objects[t]
	if o == nil {
		panic("internal error")
	}

	objectMu.Unlock()
	return o
}

func removeObject(t uintptr) {
	objectMu.Lock()
	if _, ok := objects[t]; !ok {
		panic("internal error")
	}

	delete(objects, t)
	objectMu.Unlock()
}
